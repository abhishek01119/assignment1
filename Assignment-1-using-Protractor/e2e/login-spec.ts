import { browser, element, by } from 'protractor';

describe('Login Page', function () {
  browser.ignoreSynchronization = true;
  it('Blank Credentials for password', function () {
    browser.ignoreSynchronization = true;
    browser.get('http://localhost:4200');
 

    // element(by.id('password')).sendKeys('');
    // element(by.id('username')).sendKeys('');
    // element(by.id('password')).sendKeys('');

     element(by.id('submit')).submit();
    //  browser.driver.sleep(100000);
     browser.waitForAngular();
    element(by.id('authenticationmsg'))
      .getText()
      .then(function (text) {

        expect(text).toEqual('Please enter Email and password');
      });
  });

 


  // it('Wrong E-mail format', function () {
  //   browser.get('http://localhost:4200');

  //   element(by.id('username')).sendKeys('aaa');
  
  

  //   element(by.id('submit')).click();
  //   //browser.driver.sleep(100000);
  //   browser.waitForAngular();
  //   element(by.id('authenticationmsg'))
  //     .getText()
  //     .then(function (text) {
  //       expect(text).toEqual('Please Enter password');
  //     });
  // });



  it('Password required', function () {
    browser.get('http://localhost:4200');

    element(by.id('username')).sendKeys('aaa@gmail.com');
  
  

    element(by.id('submit')).click();
    //browser.driver.sleep(100000);
    browser.waitForAngular();
    element(by.id('authenticationmsg'))
      .getText()
      .then(function (text) {
        expect(text).toEqual('Please Enter password');
      });
  });

  it('Mail format issue', function () {
    browser.get('http://localhost:4200');

    element(by.id('username')).sendKeys('aaa');
    element(by.id('password')).sendKeys('12345678');
    

    element(by.id('submit')).click();
    //browser.driver.sleep(100000);
    browser.waitForAngular();
    element(by.id('authenticationmsg'))
      .getText()
      .then(function (text) {
        expect(text).toEqual('Please enter Valid Email format(EX:abc@gmail.com)');
      });
  });


  
  it('InValid password', function () {
    browser.get('http://localhost:4200');
    element(by.id('username')).sendKeys('aaa@gmail.com');
    element(by.id('password')).sendKeys('1111111');

    

    element(by.id('submit')).click();
    //browser.driver.sleep(100000);
    browser.waitForAngular();
    element(by.id('authenticationmsg'))
      .getText()
      .then(function (text) {
        expect(text).toEqual('Password length should be atleast 8(ex:12345678)');
      });
  });

  it('Valid Credentials E-mail and password', function () {
    browser.ignoreSynchronization = true;
    browser.get('http://localhost:4200');
    browser.driver.sleep(10000);

    element(by.id('username')).sendKeys('abc@gmail.com');
    element(by.id('password')).sendKeys('12345678');

    element(by.id('submit')).click();
    //browser.driver.sleep(100000);
    browser.waitForAngular();
    element(by.id('homePage'))
      .getText()
      .then(function (text) {
        expect(text).toEqual('Welcome to homepage');
      });
  });
});
