import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { Routes, RouterModule } from '@angular/router';
import { HomePageComponent } from './home-page/home-page.component';


const appRoutes:Routes=[
  { path: '', pathMatch: 'full', redirectTo: '/login' },
  { path:'login' ,component: LoginComponent},
  { path:'homeNext' ,component: HomePageComponent}
  // { path: '**', pathMatch: 'full', redirectTo: '/login' }  
]

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
