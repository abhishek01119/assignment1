var Login = require("./login-page");

describe("Login Page", function() {

   var login = new Login();

   it("Check if credentials are valid", function() {
      expect(login.login("abc@gmail.com",12345678)).toBe(true);
   });
   it("Check if credentials are valid", function() {
      expect(login.login('','')).toBe(false);
   });
   it("Check if credentials are valid", function() {
      expect(login.login("abc@gmail.com",'')).toBe(false);
   });
   it("Check if credentials are valid", function() {
      expect(login.login("abc",12345678)).toBe(false);
   });
   it("Check if credentials are valid", function() {
      expect(login.login("aac@gmail.com",12345678)).toBe(false);
   });

   it("Check if credentials are valid", function() {
      expect(login.login("aac@gmail.com",123458)).toBe(false);
   });

  });